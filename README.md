# Camera Controller

This Python TUI program controls RTMP streams for Reolink cameras.
It starts, stops, and monitors (i.e., for crashes) camera feeds.
Camera feeds are launched using the `mpv` command for ease of access.
Camera feeds are configured using a TOML file(s) stored at `~/.config/cameras/*.toml`.
A console log is also presented to the user.

Stream definitions can be set to either: "hi" (RTMP Clear), "lo" (RTMP Fluent), or "r" (RTSP) in the TOML.

The `screen` option sets MPV to spawn on a particular monitor.

The window positioning configs (e.g., `screen`) do not work with Wayland (as of 0.38.0).
