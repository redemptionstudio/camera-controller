#!/bin/python3

import logging
import os
import sys
import psutil
import subprocess
import select
import signal
import time
import toml
import json
import curses

# Information
TITLE = "Camera Controller"
VERSION = "0.8.6"
DATE = "20240726"
AUTHOR = "T. H. Wright"
LICENSE = "GPLv3"
DESC = "Control Reolink Cameras"


def camera(title, ip, user, password, token, audio_buffer, screen, window_size, position, definition):
    if definition == "hi":
        stream = f"rtmp://{user}:{password}@{ip}/bcs/channel0_main.bcs?token={token}&channel=0&stream=0&user={user}&password={password}"
        rtsp_options = ""
    elif definition == "lo":
        stream = f"rtmp://{user}:{password}@{ip}/bcs/channel0_sub.bcs?token={token}&channel=0&stream=1&user={user}&password={password}"
        rtsp_options = ""
    elif definition == "r":
        stream = f"rtsp://{user}:{password}@{ip}/Preview_01_main"
        rtsp_options = "--rtsp-transport=tcp"
    else:
        return None
    
    mpv_command = f"mpv --title={title} --really-quiet --screen={screen} --geometry={window_size}{position} --keepaspect=no --audio-buffer={audio_buffer} --video-sync=audio --untimed --profile=low-latency --no-cache {rtsp_options} {stream}"

    return title, mpv_command


def save_camera_state():
    os.makedirs(state_dir, exist_ok=True) 
    file_path = os.path.join(state_dir, "camera_state.json")
    process_info = {camera_name: {"pid": process.pid} for camera_name, process in processes.items()}
    with open(file_path, 'w') as f:
        json.dump(process_info, f)


def start_camera(camera_name, camera_config):
    camera_info = camera(**camera_config)
    if camera_info:
        process = subprocess.Popen(["nohup"] + camera_info[1].split(), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if isinstance(process, subprocess.Popen):
            processes[camera_name] = process
            save_camera_state()
            return True, f"Started {camera_name}."
    else:
        return False, f"Failed to start camera {camera_name}."


def start_all_cameras():
    for camera_name, camera_config in camera_configs.items():
        if camera_name not in processes:
            state, message = start_camera(camera_name, camera_config)
            if not state:
                break
    return f"Started all cameras."


def stop_camera(camera_name):
    process = processes.get(camera_name)
    if process:
        if isinstance(process, subprocess.Popen):
            try:
                process.terminate()
                process.wait(timeout=5)
                processes.pop(camera_name, None)
                save_camera_state()
                return f"Stopped {camera_name}."
            except subprocess.TimeoutExpired:
                process.kill()
                processes.pop(camera_name, None)
                save_camera_state()
                return f"Stopped {camera_name} forcibly."
        else:
            return f"Failed to stop camera {camera_name}. Process not found."
    else:
        return f"Camera {camera_name} is not running."


def camera_status(camera_name):
    timestamp = time.strftime("%Y-%m-%dT%H:%M:%S")
    status = "Stopped"
    if camera_name in processes:
        if isinstance(processes[camera_name], subprocess.Popen):
            process = processes[camera_name]
            if process.poll() is None:  
                return "Running"
            else:
                del processes[camera_name]
                if restart == True:
                    restart_queue.append(camera_name)
                logging.console_log.append((timestamp, f"{camera_name} closed externally."))
    return "Stopped"


def end(signal, frame):
    for process_name, process in processes.items():
        if isinstance(process, subprocess.Popen):
            try:
                process.terminate()
                process.wait(timeout=5)
            except subprocess.TimeoutExpired:
                process.kill()
                process.wait()
    save_camera_state()
    logging.info("Exiting…")
    curses.endwin()
    sys.exit(0)


def load_camera_configs():
    global camera_configs
    camera_configs = {}
    for config_file in os.listdir(config_dir):
        if config_file.endswith(".toml"):
            config_path = os.path.join(config_dir, config_file)
            camera_config = toml.load(config_path)
            camera_name = os.path.splitext(config_file)[0] 
            camera_configs[camera_name] = camera_config
    return camera_configs


def info(stdscr):
    stdscr.erase()
    stdscr.addstr(0, 0, "# Camera Controller")
    stdscr.addstr(2, 0, "---Status---")
    total_cameras = len(camera_configs)
    if total_cameras > 0:
        for i, (camera_name, camera_config) in enumerate(camera_configs.items(), 1):
            status = camera_status(camera_name)
            if status == "Running":
                stdscr.addstr(2+i, 0, f" {i}. Deactivate {camera_name}: PID: {processes[camera_name].pid}")
            elif status == "Stopped" or camera_name not in processes:
                stdscr.addstr(2+i, 0, f" {i}. Activate {camera_name}: {status}.")
            else:
                stdscr.addstr(2+i, 0, "Error.")

        stdscr.addstr(2+(total_cameras)+1, 0, f" {total_cameras+1}. Start All.")
        stdscr.addstr(2+(total_cameras)+2, 0, f" {total_cameras+2}. Stop All.")
        stdscr.addstr(2+(total_cameras)+3, 0, f" {total_cameras+3}. Enable autorestart.")
        stdscr.addstr(2+(total_cameras)+4, 0, f" {total_cameras+4}. Exit.")
        stdscr.addstr(2+(total_cameras)+5, 0, "Choose option. [q|Q], Escape, or Ctrl+C to exit.")
    else:
        stdscr.addstr(2+total_cameras+1, 0, "No cameras detected. Exiting…")

    stdscr.addstr(2+total_cameras+6, 0, f"---Console---")
    recent_messages = logging.console_log[-3:]
    for i, (timestamp, message) in enumerate(recent_messages, 1):
        stdscr.addstr(2+total_cameras+6+i, 0, f"{timestamp} {message}")

    stdscr.noutrefresh()


config_dir = os.path.join(os.path.expanduser("~"), ".config", "cameras")
state_dir = os.path.join(os.path.expanduser("~"), ".local", "state")
log_file = os.path.join(state_dir, "camera_controller.log")
logging.basicConfig(filename=log_file, level=logging.DEBUG)
logging.console_log = []
processes = {}
camera_configs = load_camera_configs()
console_message = ""
restart = False
restart_queue = []


def main(stdscr):
    global console_message, processes, restart, restart_queue

    curses.curs_set(0) 
    stdscr.clear()
    stdscr.refresh()

    total_cameras = len(camera_configs)
    info_window_height = 16 + (total_cameras * 2)

    info_window = curses.newwin(info_window_height, curses.COLS, 0, 0)

    signal.signal(signal.SIGINT, end)
    console_message = "Starting."
    timestamp = time.strftime("%Y-%m-%dT%H:%M:%S")
    logging.console_log.append((timestamp, console_message))

    try:
        while True:
            info_window.erase() 
            info(info_window)
            timestamp = time.strftime("%Y-%m-%dT%H:%M:%S")
            stdscr.timeout(100)

            c = stdscr.getch() 

            if c == -1:
                pass
            elif c == curses.KEY_RESIZE: 
                curses.endwin()
                stdscr = curses.initscr()
                curses.curs_set(0)
                stdscr.clear()
                stdscr.refresh()
                info_window = curses.newwin(info_window_height, curses.COLS, 0, 0)
            elif c == ord('\r') or c == ord('\n'):
                console_message = "No input provided."
                logging.console_log.append((timestamp, console_message))
                pass
            elif c != -1:
                if int(ord(chr(c))) > 48:
                    c = int(ord(chr(c))) - 48
            
                if 1 <= int(ord(chr(c))) <= len(camera_configs):
                    camera_name = list(camera_configs.keys())[int(ord(chr(c))) - 1]
                    if camera_name in processes:
                        console_message = stop_camera(camera_name)
                        logging.console_log.append((timestamp, console_message))
                    else:
                        state, console_message = start_camera(camera_name, camera_configs[camera_name])
                        logging.console_log.append((timestamp, console_message))
                elif int(ord(chr(c))) == total_cameras + 1:
                    console_message = start_all_cameras()
                elif int(ord(chr(c))) == total_cameras + 2:
                    processes_bak = dict(processes)
                    if len(processes_bak) > 0:
                        for camera_name in processes_bak.keys():
                            stop_camera(camera_name)
                        console_message = "Stopped all cameras."
                        logging.console_log.append((timestamp, console_message))
                    else:
                        console_message = "No running cameras."
                        logging.console_log.append((timestamp, console_message))
                elif int(ord(chr(c))) == total_cameras + 3:
                    if restart:
                        restart = False
                        console_message = "Disabling auto-restart."
                    else:
                        console_message = "Enabling auto-restart."
                        restart = True
                    logging.console_log.append((timestamp, console_message))
                elif c == ord('q') or c == ord('Q') or c == ord('\x1b') or int(ord(chr(c))) == total_cameras+4:
                    signal.signal(signal.SIGINT, end)
                    break
            else:
                console_message = "Input unknown."
                logging.console_log.append((timestamp, console_message))
                pass

            if len(restart_queue) > 0:
                for i in restart_queue:
                    logging.console_log.append((timestamp, f"Restarting {i}"))
                    state, console_message = start_camera(camera_name, camera_configs[i])
                    logging.console_log.append((timestamp, console_message))
                    restart_queue.pop(0)


            save_camera_state()
            info_window.refresh()
    except KeyboardInterrupt:
        signal.signal(signal.SIGINT, end)

    curses.endwin()


if __name__ == "__main__":
    curses.wrapper(main)
